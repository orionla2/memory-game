const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var extractCSS = new MiniCssExtractPlugin({
  filename: '[name].css',
});
const path = require('path');
//const images = require('./src/resources/images/manifest.json');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  devtool: 'inline-source-map',
  plugins: [
    new CleanWebpackPlugin(['dist/*'], {
      watch: true,
    }),
    extractCSS,
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './src/index.html'),
    }),
  ],
  module: {
    rules: [
      // {
      //   test: /\.ts$/,
      //   use: 'ts-loader',
      // },
      // {
      //   test: /\.s[ac]ss$/,
      //   use: [
      //     {
      //       loader: MiniCssExtractPlugin.loader,
      //     },
      //     'css-loader',
      //     'sass-loader',
      //   ],
      // },
      // {
      //   test: /\.(png|jpeg|svg|gif)$/,
      //   use: [
      //     {
      //       loader: 'file-loader',
      //       options: {
      //         name: '[name].[ext]',
      //         outputPath: images.baseUrl,
      //       },
      //     },
      //   ],
      // },
      {
        test: /\.(woff|woff2)$/,
        use: 'url-loader',
      },
      // {
      //   test: /\.mp3$/,
      //   use: {
      //     loader: 'file-loader',
      //     options: {
      //       outputPath: 'sounds',
      //     },
      //   },
      // },
      {
        test: /\.glsl$/,
        loader: 'webpack-glsl-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@images': path.resolve(__dirname, 'src/resources/images'),
    },
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 8080,
  },
};
