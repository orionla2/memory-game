import { MainStage } from './main-stage';

class Game {
  constructor(displayOptions, gameOptions) {
    this.screen = {};
    this.subscribers = [];
    this.initGameState(displayOptions);
    this.initGameView(gameOptions);
  }
  
  initGameState(options) {
    const container = document.getElementById('container');
    const canvas = document.createElement('canvas');
    for (const option in options.canvas) {
      this.screen[option] = options.canvas[option]
      canvas.setAttribute(option, options.canvas[option]);
    }
    container.appendChild(canvas);
    this.canvas = canvas;
    if (canvas.getContext) this.ctx = canvas.getContext('2d');
  }

  initGameView(options) {
    new MainStage(this, options);
  }
}

const width = document.body.clientWidth * 0.8;
const height = document.body.clientHeight * 0.8;
const displayOptions = {
  canvas: {
    width,
    height,
  },
};

const gameOptions = {
  debug: false,
  gridColor: 'rgba(255, 0, 0, 0.5)',
  indentX: 10,
  indentY: 10,
  gridRows: 40,
  gridColumns: 60,
  fillSpace: {
    from: '2:2',
    to: '59:59',
  }
}

new Game(displayOptions, gameOptions);
