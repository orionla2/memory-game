import { Grid } from './grid.component';

export class MainStage {
  constructor(Game, options) {
    this.game = Game;
    this.init(options);
  }

  init(options) {
    const { indentX, indentY, gridRows, gridColumns, fillSpace, debug, gridColor } = options;
    const from = fillSpace.from.split(':');
    const to = fillSpace.to.split(':');
    this.grid = new Grid(this, {
      debug,
      gridColor,
      cords: {
        x: indentX,
        y: indentY,
      },
      size: {
        vertical: gridRows,
        horizontal: gridColumns, 
      },
      tile: {
        height: (this.game.screen.height - (indentY * 2) ) / gridRows,
        width: (this.game.screen.width - (indentX * 2) ) / gridColumns,
      },
      fillSpace: {
        fromX: from[0],
        fromY: from[1],
        toX: to[0],
        toY: to[1],
      },
    });
  }
}
