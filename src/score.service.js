export class ScoreService {
  constructor(options) {
    this.options = options;
    this.winScore = this.options.winScore;
    this.reset();
  }

  validate(card) {
    if (card.selected) return;
    if (!this.currentCardId) {
      card.switchSide();
      card.rerender();
      card.setSelected();
      this.currentCardId = card.id;
      return;
    }
    if (this.currentCardId && this.currentCardId === card.id) {
      card.switchSide();
      card.rerender();
      card.setSelected();
      this.increaseScore();
      this.currentCardId = null;
      return;
    }
    if (this.currentCardId && this.currentCardId !== card.id) {
      this.decreaseChances();
      return;
    }
  }
  
  increaseScore() {
    this.score++;
    if (this.winScore === this.score) this.win();
  }

  decreaseChances() {
    this.chances--;
    if (this.chances <= 0) this.lose();
  }

  lose() {
    alert(`Поражение с счетом ${this.score}!!!`);
    this.endGame();
  }

  win() {
    alert(`Победа с счетом ${this.score}.`);
    this.endGame();
  }

  endGame() {
    this.reset();
    this.options.resetClbk();
  }

  reset() {
    this.currentCardId = null;
    this.score = 0;
    this.chances = this.options.chances;
  }
}