import { Card } from './card.component';
import { ScoreService } from './score.service';

export class Grid {
  constructor(scene, options) {
    this.options = options;
    this.scene = scene;
    this.figures = ['square', 'circle', 'triangle', 'romb'];
    this.deck = [];
    this.cords = new Map();
    this.init();
    this.create();
  }

  init() {
    this.fillSpace = this.options.fillSpace;
    const deck = this.getListOfCards();
    const tileHeight = parseInt(this.options.tile.height);
    const tileWidth = parseInt(this.options.tile.width);
    const gridHeight = parseInt(this.options.size.vertical);
    const gridWidth = parseInt(this.options.size.horizontal);
    const startX = parseInt(this.options.cords.x);
    const startY = parseInt(this.options.cords.y);
    const ctx = this.scene.game.ctx;
    let tileX = 0 + startX;
    let tileY = 0 + startY;
    
    for (let x = 1; x <= gridWidth; x++) {
      for (let y = 1; y <= gridHeight; y++) {
        if (this.fillRequired(x, y)) {
          const card = this.getRandomElement(deck, true);
          card.setWorkspace(tileX, tileY, tileWidth, tileHeight, x, y);
          card.rerender();
          this.deck.push(card);
          this.cords.set(`${x}:${y}`, card);
        } else {
          this.cords.set(`${x}:${y}`, null);
        }
        if (this.options.debug) this.debug({ ctx, tileX, tileY, tileWidth, tileHeight });
        tileY = (tileHeight * y) + startY;
      }
      tileX = (tileWidth * x) + startX;
      tileY = 0 + startY;
    }
  }

  debug(options) {
    const { ctx, tileX, tileY, tileWidth, tileHeight } = options;
    ctx.strokeStyle = this.options.gridColor;
    ctx.strokeRect(tileX, tileY, tileWidth, tileHeight);
  }

  create() {
    this.score = new ScoreService({
      chances: 3,
      winScore: this.getAmountOfPairs(),
      resetClbk: this.reset.bind(this),
    });
    this.scene.game.canvas.addEventListener('click', (event) => {
      const x = event.pageX - this.scene.game.canvas.offsetLeft;
      const  y = event.pageY - this.scene.game.canvas.offsetTop;
      this.deck.forEach((card) => { 
        if (
          y > card.y + card.indentY && 
          y < card.y + (card.height - card.indentY * 2) && 
          x > card.x + card.indentX && 
          x < card.x + (card.width - card.indentX)
          ) {
            this.score.validate(card);
        }
      });
    }, false);
    this.initVisiblePeriod();
  }

  initVisiblePeriod() {
    setTimeout(() => {
      this.deck.forEach(card => {
        card.switchSide();
        card.rerender();
      });
    }, 5000);
  }

  update() {
    this.deck.forEach(card => {
      card.update();
    });
  }

  reset() {
    this.cords = new Map();
    this.deck.forEach(card => {
      card.destroy();
    });
    this.deck = [];
    this.init();
    this.score.reset();
    this.initVisiblePeriod();
  }

  getListOfCards() {
    const amountOfUniqPairs = this.getAmountOfPairs();
    const deck = [];
    for (let i = 0; i < amountOfUniqPairs; i++) {
      const payload = {
        mainColor: this.getRandomColor(),
        secondaryColor: this.getRandomColor(),
        mainFigure: this.getRandomElement(this.figures),
        secondaryFigure: this.getRandomElement(this.figures),
        id: this.getRandomId(),
      };
      deck.push(new Card(this.scene, payload));
      deck.push(new Card(this.scene, payload));
    }
    return deck;
  }

  getAmountOfPairs() {
    const columns = this.fillSpace.toX - this.fillSpace.fromX + 1;
    const rows = this.fillSpace.toY - this.fillSpace.fromY + 1;
    return columns * rows / 2;
  }

  fillRequired(x,y) {
    return x >= this.fillSpace.fromX && x <= this.fillSpace.toX && y >= this.fillSpace.fromY && y <= this.fillSpace.toY
  }

  getRandomColor(transparency = 0.5) {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + `, ${transparency} )`;
  }

  getRandomElement(items, needRemove = false) {
    const element = items[Math.floor(Math.random()*items.length)];
    if (needRemove) items.splice(items.indexOf(element), 1);
    return element;
  }

  getRandomId() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }
}
