export class Card {
  constructor(scene, options) {
    this.status = false;
    this.selected = false;
    this.side = 'front';
    this.scene = scene;
    this.init(options);
    this.create();
  }

  init(options) {
    this.id = options.id;
    this.mainColor = options.mainColor;
    this.secondaryColor = options.secondaryColor;
    this.mainFigure = options.mainFigure;
    this.secondaryFigure = options.secondaryFigure;
    this.x = null;
    this.y = null;
  }

  setSelected() {
    this.selected = true;
  }

  create() {
    // TODO
  }

  update() {
    if (this.status) this.render();
  }

  destroy() {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 2;
    const height = this.height - this.indentY * 2;
    const cordX = this.x + this.indentX;
    const cordY = this.y + this.indentY;
    ctx.clearRect(cordX, cordY, width, height);
  }

  rerender() {
    this.status = true;
    this.render();
  }

  switchSide() {
    this.side === 'front' ? this.side = 'back' : this.side = 'front';
  }

  setWorkspace(x, y, width, height, cordX, cordY) {
    this.cordX = cordX; 
    this.cordY = cordY;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.indentX = width * 0.2 / 2;
    this.indentY = height * 0.2 / 2;
    this.status = true;
  }

  render() {
    if (this.selected) return;
    if (this.side === 'front') {
      this.drawCard();
      this.drawFigure(this.secondaryFigure, this.secondaryColor, 'secondary');
      this.drawFigure(this.mainFigure, this.mainColor, 'main');
    } else if (this.side === 'back') {
      this.drawCard();
    }
    this.status = false;
  }

  drawFigure(figure, color, type) {
    const resolvers = {
      square: this.drawSquare,
      circle: this.drawCircle,
      romb: this.drawRomb,
      triangle: this.drawTriangle,
    };
    resolvers[figure].call(this, color, type);
  }

  drawSquare(color) {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 4;
    const height = this.height - this.indentY * 4;
    ctx.beginPath();
    let cordX = this.x + this.indentX * 2;
    let cordY = this.y + this.indentY * 2;
    ctx.moveTo(cordX, cordY);
    cordX = cordX + width;
    ctx.lineTo(cordX, cordY);
    cordY = cordY + height;
    ctx.lineTo(cordX, cordY);
    cordX = cordX - width;
    ctx.lineTo(cordX, cordY);
    ctx.fillStyle = color;
    ctx.fill();
  }

  drawCircle(color) {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 5;
    ctx.beginPath();
    let cordX = this.x + this.indentX * 5;
    let cordY = this.y + this.indentY * 5;
    ctx.arc(cordX, cordY, width / 2, 0, Math.PI * 2, true);
    ctx.fillStyle = color;
    ctx.fill();
  }

  drawRomb(color) {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 4;
    const height = this.height - this.indentY * 4;
    ctx.beginPath();
    let cordX = this.x + this.indentX * 5;
    let cordY = this.y + this.indentY * 2;
    ctx.moveTo(cordX, cordY);
    cordX = cordX + width / 2;
    cordY = cordY + height / 2;
    ctx.lineTo(cordX, cordY);
    cordX = cordX - width / 2;
    cordY = cordY + height / 2;
    ctx.lineTo(cordX, cordY);
    cordY = cordY - height / 2;
    cordX = cordX - width / 2;
    ctx.lineTo(cordX, cordY);
    ctx.fillStyle = color;
    ctx.fill();
  }

  drawTriangle(color, type) {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 4;
    const height = this.height - this.indentY * 4;
    ctx.beginPath();
    if (type == 'main') {
      let cordX = this.x + this.indentX * 5;
      let cordY = this.y + this.indentY * 2;
      ctx.moveTo(cordX, cordY);
      cordY = cordY + height;
      cordX = cordX + width / 2;
      ctx.lineTo(cordX, cordY);
      cordX = cordX - width;
      ctx.lineTo(cordX, cordY);
    } else {
      let cordX = this.x + this.indentX * 2;
      let cordY = this.y + this.indentY * 2;
      ctx.moveTo(cordX, cordY);
      cordX = cordX + width;
      ctx.lineTo(cordX, cordY);
      cordY = cordY + height;
      cordX = cordX - width / 2;
      ctx.lineTo(cordX, cordY);
    }
    ctx.fillStyle = color;
    ctx.fill();
  }

  drawCard() {
    const ctx = this.scene.game.ctx;
    const width = this.width - this.indentX * 2;
    const height = this.height - this.indentY * 2;
    ctx.beginPath();
    let cordX = this.x + this.indentX;
    let cordY = this.y + this.indentY;
    ctx.clearRect(cordX, cordY, width, height);
    ctx.moveTo(cordX, cordY);
    cordX = cordX + width;
    ctx.lineTo(cordX, cordY);
    cordY = cordY + height;
    ctx.lineTo(cordX, cordY);
    cordX = cordX - width;
    ctx.lineTo(cordX, cordY);
    cordY = cordY - height;
    ctx.lineTo(cordX, cordY);
    if (this.side === 'front') {
      ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
    } else if (this.side === 'back') {
      ctx.fillStyle = 'rgba(178, 200, 14, 0.7)';
    }
    ctx.fill();
    ctx.strokeStyle = 'rgba(0, 0, 0, 0.5)'
    ctx.stroke();
  }
}
